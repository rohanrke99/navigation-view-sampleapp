package com.lalamove.myapplication

import android.os.Bundle
import android.view.Gravity
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.view.Menu
import android.view.View
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.lalamove.myapplication.databinding.ActivityMainBinding
import com.lalamove.myapplication.ui.home.HomeFragment
import com.lalamove.myapplication.ui.send.SendFragment
import com.lalamove.myapplication.ui.share.ShareFragment

class MainActivity : AppCompatActivity() ,OnNavigationClickListener{

    private lateinit var binding : ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       binding = DataBindingUtil.setContentView(this,R.layout.activity_main)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        val fab: FloatingActionButton = findViewById(R.id.fab)
        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }

        ///Binding click listener to custom view of navigation view
        binding.listener = this

    }


    // Listener for custom views

    override fun onItemClick(view: View) {
        when(view.id){

            R.id.tv1-> {
                navigateToItemFragment(HomeFragment.getInstance())
            }
            R.id.tv2-> {
                navigateToItemFragment(SendFragment.getInstance())
            }
            R.id.tv3-> {
                navigateToItemFragment(ShareFragment.getInstance())
            }
        }
    }

    private fun navigateToItemFragment(fragment: Fragment){


        // Or open fragment in new Activity
        // Use add in place of replace to maintain back button flow

        supportFragmentManager.beginTransaction().replace(R.id.frame,fragment)
            .commit()
        binding.drawerLayout.closeDrawer(Gravity.LEFT)
    }
}
