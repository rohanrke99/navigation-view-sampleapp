package com.lalamove.myapplication

import android.view.View

interface OnNavigationClickListener {

    fun onItemClick(view : View)
}